WIN Pandas educational session
==============================


This session is designed to introduce you to `pandas`, a Python library for
working with tabular data.


The session takes the form of an interactive Jupyter notebook - we will work
through the notebook as a group, running the code and inspecting its results.


To get started:

1. Clone this git repository on your laptop:

   ```bash
   git clone https://git.fmrib.ox.ac.uk/ndcn0236/pandas-tutorial.git
   cd pandas-tutorial
   ```


2. Create a Python environment to run the notebook in (you need to have FSL 5.0.10 or newer installed to run this.):

   ```bash
   $FSLDIR/fslpython/bin/conda create python=3.6 pandas jupyter notebook seaborn statsmodels scipy -p ./myenv
   source $FSLDIR/fslpython/bin/activate ./myenv
   ```


3. Start the notebook. This will open the notebook in a web browser tab:

   ```bash
   jupyter notebook pandas.ipynb
   ```


You're ready to go!
